const path = require("path")
const nodeExternals = require("webpack-node-externals")
const webpack = require("webpack")

module.exports = {
    target: "node",
    externals: [nodeExternals()],
    mode: "production",
    entry: ["./app/index.js"],
    output: {
        path: path.resolve(__dirname, "../dist"),
        filename:"bundle.js",
    },
    resolve: {
        modules: ["node_modules"],
    },
    plugins: [
        new webpack.BannerPlugin({ banner: "#!/usr/bin/env node", raw: true }),
    ],
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                loader: "babel-loader",
                options: {
                    presets: ["@babel/preset-env"],
                },
            },
        ],
    },
    watchOptions: {
        ignored: ["node_modules"],
    },
}
