install:
	npm install

build:
	npm run webpack:build

link:
	npm link --force

unlink:
	npm unlink

dev: install build link
	npm run webpack:dev

test: build link
	npm run dev:test

coverage:
	npm run test:coverage

clean: unlink
	rm -rf node_modules
	rm -rf dist
	rm -rf coverage
	rm -rf .npmrc
	rm -rf .csslintrc
	rm -rf .rubocop.yml
	rm -rf coffeelint.json

create-npmrc: clean
	echo "commit-hooks=false" >> .npmrc
	echo "@g6s:registry=https://gitlab.com/api/v4/projects/24106879/packages/npm/" >> .npmrc
	echo "//gitlab.com/api/v4/projects/24106879/packages/npm/:_authToken=${GITLAB_PERSONAL_TOKEN}" >> .npmrc
	echo "//gitlab.com/api/v4/packages/npm/:_authToken=${GITLAB_PERSONAL_TOKEN}" >> .npmrc

# create by default by registry.gitlab.com/gitlab-org/ci-cd/codequality:latest
# docker container run --rm -it --entrypoint cat registry.gitlab.com/gitlab-org/ci-cd/codequality:latest run.sh
code-quality:
	docker container run \
	--rm \
	--env SOURCE_CODE="$$PWD" \
	--volume "$$PWD":/code \
	--volume /var/run/docker.sock:/var/run/docker.sock \
	registry.gitlab.com/gitlab-org/ci-cd/codequality:latest /code
	rm -rf .csslintrc .rubocop.yml coffeelint.json

code-quality-list:
	docker container run \
	--rm \
	codeclimate/codeclimate:0.85.22 engines:list

code-quality-validate:
	docker container run \
	--rm \
	--volume "$$PWD":/code \
	codeclimate/codeclimate:0.85.22 validate-config /code/.codeclimate.yml
