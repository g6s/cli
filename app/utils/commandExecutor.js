import { exec } from "child_process"
import path from "path"
import joi from "joi"
import { APP_BUILD_PATH } from "../const"
import validateParam from "./validateParam"

export const CREATE_COMMAND_PARAM_ARGS_DEFAULT = []
export const CREATE_COMMAND_PARAM_ARGS_SCHEMA = joi.array().items(joi.string()).required()
export const CREATE_COMMAND_ERROR_PARAMS_ARGS_TYPE = "Type must be an array of string"

export const createCommand = (args = CREATE_COMMAND_PARAM_ARGS_DEFAULT) => {
    try {
        const argsValidated = validateParam("args", CREATE_COMMAND_PARAM_ARGS_SCHEMA, args)
        return `node ${path.resolve(APP_BUILD_PATH)} ${argsValidated.join(" ")}`
    } catch (error) {
        throw error
    }
}

export const COMMAND_EXECUTOR_CALLBACK_PARAM_DONE_SCHEMA = joi.function().required()

export const commandExecutorCallback = done => {
    try {
        const doneValideted = validateParam("done", COMMAND_EXECUTOR_CALLBACK_PARAM_DONE_SCHEMA, done)
        return (error, stdout, stderr) => {
            doneValideted({
                code: error && error.code ? error.code : 0,
                error,
                stdout,
                stderr,
            })
        }
    } catch (error) {
        throw error
    }
}

export const COMMAND_EXECUTOR_PARAM_OPTS_DEFAULT = { cwd: "." }
export const COMMAND_EXECUTOR_CALLBACK_ERROR_MISSING_CALLBACK = "Missing callback in params"
export const COMMAND_EXECUTOR_CALLBACK_ERROR_INVALID_CALLBACK = "Callback need to be a instance of function"

const commandExecutor = (
    args,
    opts = COMMAND_EXECUTOR_PARAM_OPTS_DEFAULT,
    createCommandFn = createCommand,
    callbackFn = commandExecutorCallback,
) => new Promise((resolve, reject) => {
    try {
        const command = createCommandFn(args)
        exec(command, opts, callbackFn(resolve))
    } catch (error) {
        reject(error)
    }
})

export default commandExecutor
