import validateParam from "./validateParam"
import {
    MissingParamError,
    TypeOfParamError,
    InstanceOfParamError,
    ParamValidationError,
} from "./errorClass"

import joi from "joi"

describe("validateParam method", () => {
    const name = "faker"
    const value = ["faker"]
    const shema = joi.array().items(joi.string().required()).required()

    test("Should be an instance of function", () => {
        expect(validateParam).toBeInstanceOf(Function)
    })

    test("Should throw an MissingParamError if name param is missing", () => {
        expect(() => validateParam()).toThrow(new MissingParamError("name"))
    })

    test("Should throw an TypeOfParamError if type of name param is not equal to string", () => {
        expect(() => validateParam(1)).toThrow(new TypeOfParamError("name", "String"))
    })

    test("Should throw an MissingParamError if schema param is missing", () => {
        expect(() => validateParam(name)).toThrow(new MissingParamError("schema"))
    })

    test("Should throw an InstanceOfParamError if instance of schema param is not joi.ObjectSchema", () => {
        expect(() => validateParam(name, {})).toThrow(new InstanceOfParamError("schema", "joi.constructor"))
    })

    test("Should call validate method of schema param with value param and options", () => {
        const schema = joi.array()
        schema.validate = jest.fn(() => ({}))
        validateParam(name, schema, value)
        expect(schema.validate).toBeCalledWith(value, { abortEarly: true })
    })

    test("Should throw an ParamValidationError when result of validate method has an error property", () => {
        expect(() => validateParam(name, shema, undefined)).toThrow(new ParamValidationError(name, "\"faker\" is required"))
        expect(() => validateParam(name, shema, "faker")).toThrow(new ParamValidationError(name, "\"faker\" must be an array"))
        expect(() => validateParam(name, shema, [1])).toThrow(new ParamValidationError(name, "\"[0]\" must be a string"))
    })

    test("Should return value", () => {
        expect(validateParam(name, shema, value)).toEqual(value)
    })
})
