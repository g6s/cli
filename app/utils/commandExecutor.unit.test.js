import childProcess from "child_process"
import path from "path"
import joi from "joi"
import { APP_BUILD_PATH } from "../const"
import typeOf from "./typeOf"
import commandExecutor, {
    CREATE_COMMAND_PARAM_ARGS_DEFAULT,
    CREATE_COMMAND_PARAM_ARGS_SCHEMA,
    CREATE_COMMAND_ERROR_PARAMS_ARGS_TYPE,
    createCommand,
    COMMAND_EXECUTOR_CALLBACK_PARAM_DONE_SCHEMA,
    commandExecutorCallback,
    COMMAND_EXECUTOR_PARAM_OPTS_DEFAULT,
    COMMAND_EXECUTOR_CALLBACK_ERROR_MISSING_CALLBACK,
    COMMAND_EXECUTOR_CALLBACK_ERROR_INVALID_CALLBACK,
} from "./commandExecutor"
import validateParam from "./validateParam"

jest.mock("./validateParam")

describe("createCommand method", () => {
    describe("Constants", () => {
        test("Should have a CREATE_COMMAND_PARAM_ARGS_DEFAULT equal to empty array", () => {
            expect(CREATE_COMMAND_PARAM_ARGS_DEFAULT).toStrictEqual([])
        })

        test("Should have a CREATE_COMMAND_PARAM_ARGS_SCHEMA equal to an joi schema", () => {
            expect(JSON.stringify(CREATE_COMMAND_PARAM_ARGS_SCHEMA))
            .toStrictEqual(JSON.stringify(joi.array().items(joi.string()).required()))
        })

        test("Should have a CREATE_COMMAND_ERROR_PARAMS_ARGS_TYPE with a message", () => {
            expect(CREATE_COMMAND_ERROR_PARAMS_ARGS_TYPE).toEqual("Type must be an array of string")
        })
    })

    const name = "args"
    const args = ["faker"]

    /** @type {jest.SpyInstance} */
    let typeOfTestSpy

    beforeEach(() => {
        typeOfTestSpy = jest.spyOn(typeOf, "test")
        validateParam.mockImplementation(() => args)
    })

    afterEach(() => {
        typeOfTestSpy.mockRestore()
        validateParam.mockReset()
    })

    test("Should be a instance of function", () => {
        expect(createCommand).toBeInstanceOf(Function)
    })

    test("Should call validateParam with name, CREATE_COMMAND_PARAM_ARGS_SCHEMA and args param", () => {
        createCommand(args)
        expect(validateParam).toBeCalledWith(name, CREATE_COMMAND_PARAM_ARGS_SCHEMA, args)
    })

    test("Should have a default value for args param equal to CREATE_COMMAND_PARAM_ARGS_DEFAULT", () => {
        createCommand()
        expect(validateParam).toBeCalledWith(name, CREATE_COMMAND_PARAM_ARGS_SCHEMA, CREATE_COMMAND_PARAM_ARGS_DEFAULT)
    })

    test("Should throw an Error when validateParam method throw", () => {
        const error = new Error("faker")
        validateParam.mockImplementation(() => { throw error })
        expect(() => createCommand("faker")).toThrow(error)
    })

    test("Should call join method of param args with ` `", () => {
        const argsSpy = jest.spyOn(args, "join")
        createCommand(args)
        expect(argsSpy).toBeCalledWith(" ")
    })

    test("Should call resolve method of path args with APP_BUILD_PATH", () => {
        const pathResolveSpy = jest.spyOn(path, "resolve")
        createCommand(args)
        expect(pathResolveSpy).toBeCalledWith(APP_BUILD_PATH)
        pathResolveSpy.mockRestore()
    })

    test("Should return command", () => {
        const expected = `node ${path.resolve(APP_BUILD_PATH)} ${args.join(" ")}`
        expect(createCommand(args)).toEqual(expected)
    })
})

describe("commandExecutorCallback method", () => {
    describe("Constants", () => {
        test("Should have a COMMAND_EXECUTOR_PARAM_OPTS_DEFAULT equal to `{ cwd: '.' }`", () => {
            expect(COMMAND_EXECUTOR_PARAM_OPTS_DEFAULT).toStrictEqual({ cwd: "." })
        })
    })

    const returnValue = {
        code: 0,
        error: null,
        stdout: "faker stdout",
        stderr: "faker stderr",
    }

    /** @type {jest.Mock} */
    let callbackMock

    beforeEach(() => {
        callbackMock = jest.fn()
        validateParam.mockImplementation(() => callbackMock)
    })

    afterEach(() => {
        callbackMock.mockReset()
    })

    test("Should be a instance of function", () => {
        expect(commandExecutorCallback).toBeInstanceOf(Function)
    })

    test("Should call validateParam with 'joi.function().required()' and args param", () => {
        commandExecutorCallback(callbackMock)
        expect(validateParam).toBeCalledWith("done", COMMAND_EXECUTOR_CALLBACK_PARAM_DONE_SCHEMA, callbackMock)
    })

    test("Should throw an Error when validateParam method throw", () => {
        const error = new Error("faker")
        validateParam.mockImplementation(() => { throw error })
        expect(() => createCommand("faker")).toThrow(error)
    })

    test("Should return a function", () => {
        expect(commandExecutorCallback(callbackMock)).toBeInstanceOf(Function)
    })

    test("Should call callback in wrapped function", () => {
        commandExecutorCallback(callbackMock)()
        expect(callbackMock).toBeCalledTimes(1)
    })

    test("Should return an object with properties code and all params in callback function", () => {
        commandExecutorCallback(callbackMock)(returnValue.error, returnValue.stdout, returnValue.stderr)
        expect(callbackMock).toBeCalledWith(returnValue)
    })

    test("Should return a error code in callback function when error param exist and has a error code", () => {
        const expectedError = new Error("faker")
        expectedError.code = 12
        const expected = {
            ...returnValue,
            error: expectedError,
            code: expectedError.code,
        }
        commandExecutorCallback(callbackMock)(expected.error, expected.stdout, expected.stderr)
        expect(callbackMock).toBeCalledWith(expected)
    })
})

describe("commandExecutor method", () => {
    describe("Constants", () => {
        test("Should have a COMMAND_EXECUTOR_PARAM_OPTS_DEFAULT equal to `{ cwd: '.' }`", () => {
            expect(COMMAND_EXECUTOR_PARAM_OPTS_DEFAULT).toStrictEqual({ cwd: "." })
        })

        test("Should have a COMMAND_EXECUTOR_CALLBACK_ERROR_MISSING_CALLBACK with a message", () => {
            expect(COMMAND_EXECUTOR_CALLBACK_ERROR_MISSING_CALLBACK).toStrictEqual("Missing callback in params")
        })

        test("Should have a COMMAND_EXECUTOR_CALLBACK_ERROR_INVALID_CALLBACK with a message", () => {
            expect(COMMAND_EXECUTOR_CALLBACK_ERROR_INVALID_CALLBACK).toStrictEqual("Callback need to be a instance of function")
        })
    })

    const defaultArgs = ["-V"]

    /** @type {jest.SpyInstance} */
    let execSpy
    /** @type {jest.Mock} */
    let createCommandMock
    /** @type {jest.Mock} */
    let commandExecutorCallbackMock

    const expectedResolve = "commandExecutorCallbackWrappedMock"
    const commandExecutorCallbackWrappedMock = resolve => resolve(expectedResolve)

    beforeEach(() => {
        execSpy = jest.spyOn(childProcess, "exec")
        execSpy.mockImplementation((command, opts, callback) => callback())
        createCommandMock = jest.fn(() => "faker command")
        commandExecutorCallbackMock = jest.fn(resolve => commandExecutorCallbackWrappedMock(resolve))
    })

    afterEach(() => {
        execSpy.mockRestore()
        createCommandMock.mockReset()
        commandExecutorCallbackMock.mockReset()
    })

    test("Should be a instance of function", () => {
        expect(commandExecutor).toBeInstanceOf(Function)
    })

    test("Should return a Promise", () => {
        expect(commandExecutor(
            defaultArgs,
            COMMAND_EXECUTOR_PARAM_OPTS_DEFAULT,
            createCommandMock,
            commandExecutorCallbackMock,
        )).toBeInstanceOf(Promise)
    })

    test("Should call createCommand method with param args", async () => {
        await commandExecutor(
            defaultArgs,
            COMMAND_EXECUTOR_PARAM_OPTS_DEFAULT,
            createCommandMock,
            commandExecutorCallbackMock,
        )
        expect(createCommandMock).toBeCalledWith(defaultArgs)
    })

    test("Should reject a error when createCommand command throw", async () => {
        const expected = new Error("faker error")
        createCommandMock = jest.fn(() => { throw expected })
        await expect(commandExecutor(
            defaultArgs,
            COMMAND_EXECUTOR_PARAM_OPTS_DEFAULT,
            createCommandMock,
            commandExecutorCallbackMock,
        )).rejects.toStrictEqual(expected)
    })

    test("Should call exec method of child_process", async () => {
        await commandExecutor(
            defaultArgs,
            COMMAND_EXECUTOR_PARAM_OPTS_DEFAULT,
            createCommandMock,
            commandExecutorCallbackMock,
        )
        expect(execSpy).toBeCalledTimes(1)
    })

    test("Should call exec method with command, cwd and a callback ", async () => {
        await commandExecutor(
            defaultArgs,
            COMMAND_EXECUTOR_PARAM_OPTS_DEFAULT,
            createCommandMock,
            commandExecutorCallbackMock,
        )
        expect(execSpy).toBeCalledWith(
            createCommandMock(),
            COMMAND_EXECUTOR_PARAM_OPTS_DEFAULT,
            commandExecutorCallbackMock(() => {}),
        )
    })

    test("Should resolve result of exec callback", async () => {
        await expect(commandExecutor(
            defaultArgs,
            COMMAND_EXECUTOR_PARAM_OPTS_DEFAULT,
            createCommandMock,
            commandExecutorCallbackMock,
        )).resolves.toEqual(expectedResolve)
    })
})
