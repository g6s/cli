import {
    ParamValidationError,
    MissingParamError,
    TypeOfParamError,
    InstanceOfParamError,
    ERROR_MESSAGE_MISSING,
} from "./errorClass"

describe("Constants", () => {
    test("Should have a ERROR_MESSAGE_MISSING with a message", () => {
        expect(ERROR_MESSAGE_MISSING).toEqual("Missing message when constructor was called")
    })
})

describe("ParamValidationError class", () => {
    test("Should extend Error class", () => {
        expect(new ParamValidationError()).toBeInstanceOf(Error)
    })

    test("Should have default message equal to ERROR_MESSAGE_MISSING constant", () => {
        const error = new ParamValidationError()
        expect(error.message).toEqual(ERROR_MESSAGE_MISSING)
    })

    test("Should have name property equal to 'ParamValidationError'", () => {
        const error = new ParamValidationError()
        expect(error.name).toEqual("ParamValidationError")
    })

    test("Should have param property equal to first param of constructor method", () => {
        const paramName = "faker"
        const error = new ParamValidationError(paramName)
        expect(error.param).toEqual(paramName)
    })

    test("Should have message property equal to second param of constructor method", () => {
        const message = "faker message"
        const error = new ParamValidationError(null, message)
        expect(error.message).toEqual(message)
    })
})

describe("MissingParamError class", () => {
    test("Should extend Error class", () => {
        expect(new MissingParamError()).toBeInstanceOf(Error)
    })

    test("Should have default message equal to 'Missing ??? param' when name param is undefined", () => {
        const error = new MissingParamError()
        expect(error.message).toEqual("Missing ??? param")
    })

    test("Should have default message equal to 'Missing ${name} param' when message param is undefined", () => {
        const name = "faker"
        const error = new MissingParamError(name)
        expect(error.message).toEqual(`Missing ${name} param`)
    })

    test("Should have name property equal to 'MissingParamError'", () => {
        const error = new MissingParamError()
        expect(error.name).toEqual("MissingParamError")
    })

    test("Should have param property equal to first param of constructor method", () => {
        const paramName = "faker"
        const error = new MissingParamError(paramName)
        expect(error.param).toEqual(paramName)
    })

    test("Should have message property equal to second param of constructor method", () => {
        const message = "faker message"
        const error = new MissingParamError(null, message)
        expect(error.message).toEqual(message)
    })
})

describe("TypeOfParamError class", () => {
    test("Should extend Error class", () => {
        expect(new TypeOfParamError()).toBeInstanceOf(Error)
    })

    test("Should have default message equal to 'Type of ??? is invalid. Type expected is ???' when name param and expected are undefined", () => {
        const error = new TypeOfParamError()
        expect(error.message).toEqual("Type of ??? is invalid. Type expected is ???")
    })

    test("Should have default message equal to 'Type of ${name} is invalid. Type expected is ???' when expected param is undefined", () => {
        const name = "faker"
        const error = new TypeOfParamError(name)
        expect(error.message).toEqual(`Type of ${name} is invalid. Type expected is ???`)
    })

    test("Should have default message equal to 'Type of ${name} is invalid. Type expected is ${expected}' when message param is undefined", () => {
        const name = "faker"
        const expected = "fakerType"
        const error = new TypeOfParamError(name, expected)
        expect(error.message).toEqual(`Type of ${name} is invalid. Type expected is ${expected}`)
    })

    test("Should have message property equal to second param of constructor method", () => {
        const message = "faker message"
        const error = new TypeOfParamError(null, null, message)
        expect(error.message).toEqual(message)
    })

    test("Should have name property equal to 'TypeOfParamError'", () => {
        const error = new TypeOfParamError()
        expect(error.name).toEqual("TypeOfParamError")
    })

    test("Should have param property equal to first param of constructor method", () => {
        const paramName = "faker"
        const error = new TypeOfParamError(paramName)
        expect(error.param).toEqual(paramName)
    })
})

describe("InstanceOfParamError class", () => {
    test("Should extend Error class", () => {
        expect(new InstanceOfParamError()).toBeInstanceOf(Error)
    })

    test("Should have default message equal to 'Instance of ??? is invalid. Type expected is ???' when name param and expected are undefined", () => {
        const error = new InstanceOfParamError()
        expect(error.message).toEqual("Instance of ??? is invalid. Type expected is ???")
    })

    test("Should have default message equal to 'Instance of ${name} is invalid. Type expected is ???' when expected param is undefined", () => {
        const name = "faker"
        const error = new InstanceOfParamError(name)
        expect(error.message).toEqual(`Instance of ${name} is invalid. Type expected is ???`)
    })

    test("Should have default message equal to 'Instance of ${name} is invalid. Type expected is ${expected}' when message param is undefined", () => {
        const name = "faker"
        const expected = "fakerType"
        const error = new InstanceOfParamError(name, expected)
        expect(error.message).toEqual(`Instance of ${name} is invalid. Type expected is ${expected}`)
    })

    test("Should have message property equal to second param of constructor method", () => {
        const message = "faker message"
        const error = new InstanceOfParamError(null, null, message)
        expect(error.message).toEqual(message)
    })

    test("Should have name property equal to 'InstanceOfParamError'", () => {
        const error = new InstanceOfParamError()
        expect(error.name).toEqual("InstanceOfParamError")
    })

    test("Should have param property equal to first param of constructor method", () => {
        const paramName = "faker"
        const error = new InstanceOfParamError(paramName)
        expect(error.param).toEqual(paramName)
    })
})
