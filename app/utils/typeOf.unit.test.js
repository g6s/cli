import typeOfOriginal from "typeof"
import typeOf, { ARRAY_TYPE, STRING_TYPE, OBJECT_TYPE, FUNCTION_TYPE } from "./typeOf"

jest.mock("typeof", () => {
    const original = jest.requireActual("typeof")
    return {
       __esModule: true,
       default: jest.fn(original),
    }
})

describe("Constants", () => {
    test("Should have a ARRAY_TYPE equal to array", () => {
        expect(ARRAY_TYPE).toEqual("array")
    })

    test("Should have a STRING_TYPE equal to string", () => {
        expect(STRING_TYPE).toEqual("string")
    })

    test("Should have a OBJECT_TYPE equal to object", () => {
        expect(OBJECT_TYPE).toEqual("object")
    })

    test("Should have a FUNCTION_TYPE equal to function", () => {
        expect(FUNCTION_TYPE).toEqual("function")
    })
})

describe("test method", () => {
    test("Should call default method of typeOf depedencie module", () => {
        typeOf.test("test")
        expect(typeOfOriginal).toBeCalledWith("test")
    })
})

describe("default export", () => {
    test("Should return an instance of object", () => {
        expect(typeOfOriginal(typeOf)).toEqual("object")
    })

    test("Should return an object with all methods", () => {
        expect(typeOf).toStrictEqual({
            test: typeOf.test,
        })
    })
})
