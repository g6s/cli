export const ERROR_MESSAGE_MISSING = "Missing message when constructor was called"

export class ParamValidationError extends Error {
    constructor(name, message) {
        super(message || ERROR_MESSAGE_MISSING)
        this.name = "ParamValidationError"
        this.param = name
    }
}

export class MissingParamError extends Error {
    constructor(name, message) {
        super(message || `Missing ${name || "???" } param`)
        this.name = "MissingParamError"
        this.param = name
    }
}

export class TypeOfParamError extends Error {
    constructor(name, expected, message) {
        super(message || `Type of ${name || "???" } is invalid. Type expected is ${ expected || "???" }`)
        this.name = "TypeOfParamError"
        this.param = name
    }
}

export class InstanceOfParamError extends Error {
    constructor(name, expected, message) {
        super(message || `Instance of ${name || "???" } is invalid. Type expected is ${ expected || "???" }`)
        this.name = "InstanceOfParamError"
        this.param = name
    }
}
