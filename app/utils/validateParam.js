import joi from "joi"
import {
    MissingParamError,
    TypeOfParamError,
    InstanceOfParamError,
    ParamValidationError,
} from "./errorClass"
import typeOf, { STRING_TYPE } from "./typeOf"

/**
 * @param {string} name
 * @param {any} value
 * @param {joi.ObjectSchema} schema
 */
const validateParam = (name, schema, value) => {
    if (!name) throw new MissingParamError("name")
    if (typeOf.test(name) !== STRING_TYPE) throw new TypeOfParamError("name", "String")
    if (!schema) throw new MissingParamError("schema")
    if (!joi.isSchema(schema)) throw new InstanceOfParamError("schema", "joi.constructor")

    const res = schema.validate(value, { abortEarly: true })
    if (res.error)
        throw new ParamValidationError(name, res.error.details[0].message.replace("value", name))

    return res.value
}

export default validateParam
