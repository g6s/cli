import typeOf from "typeof"

export const ARRAY_TYPE = "array"
export const STRING_TYPE = "string"
export const OBJECT_TYPE = "object"
export const FUNCTION_TYPE = "function"

export const test = arg => typeOf(arg)

export default { test }
