import { program } from "commander"
import packageJSON from "../package.json"

program.version(packageJSON.version)

program.parse()
