import { APP_BUILD_PATH } from "./const"


describe("Constants", () => {
    test("Should have a APP_BUILD_PATH equal to ./dist/bundle.js", () => {
        expect(APP_BUILD_PATH).toEqual("./dist/bundle.js")
    })
})
