import commandExecutor from "./utils/commandExecutor"
import packageJSON from "../package.json"

describe("Command: g6s -V", () => {
    test("Should prompt version in package.jsson", async () => {
        const result = await commandExecutor(["-V"])
        expect(result.code).toEqual(0)
        expect(result.stdout).toEqual(`${packageJSON.version}\n`)
    })
})
